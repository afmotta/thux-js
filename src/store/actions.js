import axios from 'axios';

export const COURSE_LIST = 'COURSE_LIST';
export const COURSE_DETAIL = 'COURSE_DETAIL';

export const getList = () => ({
  type: COURSE_LIST,
  payload: axios.get(
    'http://whistle.thux.it/api/frontend/profile/company/',
  ),
});

export const setDetail = detail => (
  { type: COURSE_DETAIL, payload: detail }
);
