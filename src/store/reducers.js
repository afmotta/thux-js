import { COURSE_LIST, COURSE_DETAIL } from './actions';

const initialState = {
  list: [],
  detail: {},
};

const courseApp = (state = initialState, { type, payload }) => {
  switch (type) {
    case COURSE_LIST:
      return { ...state, list: payload.data.results };
    case COURSE_DETAIL:
      return { ...state, detail: payload };
    default:
      return state;
  }
};

export default courseApp;
