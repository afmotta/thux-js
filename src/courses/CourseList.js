import React from 'react';

const CourseList = (props) => {
    const { list, selectDetail, detailId } = props;
    return (
        <ul>
            {list.map(course => (
                <li
                    key={course.id}
                    onClick={() => selectDetail(course)}
                    className={detailId === course.id ? 'bold' : ''}
                >
                    {course.name}
                </li>
            ))}
        </ul>
    );
};

CourseList.defaultProps = {
    list: [],
}

export default CourseList;
