import 'whatwg-fetch';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import CourseList from './CourseList';
import CourseDetail from './CourseDetail';
import './Courses.css';
import { setDetail, getList } from '../store/actions';

const Wrapper = styled.div`
    color: black;
`;

class Courses extends Component {
  componentWillMount() {
    this.props.getList();
  }

  render() {
    if (this.props.list.length === 0) {
      return <h1>Loading...</h1>;
    }
    return (
      <Wrapper>
        <h1>Companies</h1>
        <div className="block list">
          <CourseList
            list={this.props.list}
            detailId={this.props.detail.id}
            selectDetail={detail => this.props.setDetail(detail)}
          />
        </div>
        <div className="block detail">
          <CourseDetail {...this.props.detail} />
        </div>
      </Wrapper>
    );
  }
}

const mapStateToProps = state => ({
  list: state.list,
  detail: state.detail,
});

const mapDispatchToProps = dispatch => ({
  setDetail: detail => dispatch(setDetail(detail)),
  getList: () => dispatch(getList()),
});


export default connect(mapStateToProps, mapDispatchToProps)(Courses);
