import React from 'react';

const CourseDetail = (props) => {
    if (!props.name) {
        return <h2>Select a course...</h2>;
    }
    return (
        <div>
            <h1>{props.name}</h1>
            <h2>Id: {props.id}</h2>
        </div>
    );
};

export default CourseDetail;